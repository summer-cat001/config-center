package com.config.center.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author summer
 * @date 2022-04-21 16:19
 */
@Getter
@AllArgsConstructor
public enum ResultEnum {

    SUCCESS(0, "success"),
    FAIL(1, "fail");

    private int code;
    private String message;
}
