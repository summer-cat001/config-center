package com.config.center.model;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * @author caojiancheng
 * @date 2024-02-08 17:34
 */
@Data
public class ConfigVO {
    /**
     * 配置id
     */
    private long id;

    /**
     * 配置名
     */
    private String name;

    /**
     * 配置版本号
     */
    private int version;

    /**
     * 配置内容
     */
    private JSONObject configData;

    /**
     * 创建时间
     */
    private String createTime;
}
