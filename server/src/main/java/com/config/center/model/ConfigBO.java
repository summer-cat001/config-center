package com.config.center.model;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author caojiancheng
 * @date 2024-02-08 17:30
 */
@Data
public class ConfigBO {

    /**
     * 配置id
     */
    private long id;

    /**
     * 配置名
     */
    private String name;

    /**
     * 配置版本号
     */
    private int version;

    /**
     * 配置内容
     */
    private JSONObject configData;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
