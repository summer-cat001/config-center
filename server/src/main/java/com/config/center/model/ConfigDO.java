package com.config.center.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author caojiancheng
 * @date 2024-02-08 15:15
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ConfigDO extends BaseDO {
    /**
     * 配置名
     */
    private String name;

    /**
     * 配置内容
     */
    private String configData;

    /**
     * 配置版本号
     */
    private int version;
}
