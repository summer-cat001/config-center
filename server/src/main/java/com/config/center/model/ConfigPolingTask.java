package com.config.center.model;

import lombok.Data;

import javax.servlet.AsyncContext;
import java.util.Map;

/**
 * @author caojiancheng
 * @date 2024-03-05 14:55
 */
@Data
public class ConfigPolingTask {
    /**
     * 截止时间
     */
    private long endTime;

    /**
     * 异步请求
     */
    private AsyncContext asyncContext;

    /**
     * 配置轮询数据（配置id，版本）
     */
    private Map<Long, Integer> configPolingDataMap;
}
