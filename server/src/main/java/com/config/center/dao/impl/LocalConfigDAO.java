package com.config.center.dao.impl;

import com.alibaba.fastjson.JSON;
import com.config.center.dao.ConfigDAO;
import com.config.center.model.ConfigDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * @author caojiancheng
 * @date 2024-02-08 15:34
 */
@Slf4j
@Repository
public class LocalConfigDAO implements ConfigDAO {

    private final Lock insertLock = new ReentrantLock();

    @Value("${config.center.standalone.path}")
    private String standalonePath;

    @Override
    public long insertConfigDO(ConfigDO configDO) {
        insertLock.lock();
        try {
            long id = 1;
            List<ConfigDO> configList = getAllConfig();
            if (!configList.isEmpty()) {
                id = configList.get(configList.size() - 1).getId() + 1;
            }
            configDO.setId(id);
            configDO.setVersion(1);
            Optional.of(configDO).filter(c -> c.getCreateTime() == null).ifPresent(c -> c.setCreateTime(LocalDateTime.now()));

            String configPathStr = standalonePath + "/config";
            Files.createDirectories(Paths.get(configPathStr));
            Path path = Paths.get(configPathStr + "/" + id + ".conf");
            Files.write(path, JSON.toJSONString(configDO).getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE_NEW);
            return id;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            insertLock.unlock();
        }
    }

    @Override
    public void updateConfig(ConfigDO configDO) {
        ConfigDO dbConfigDO = getConfig(configDO.getId());
        Optional.ofNullable(dbConfigDO).map(c -> {
            c.setName(configDO.getName());
            c.setVersion(c.getVersion() + 1);
            c.setUpdateTime(LocalDateTime.now());
            c.setUpdateUid(configDO.getUpdateUid());
            c.setConfigData(configDO.getConfigData());
            return c;
        }).ifPresent(this::updateConfigDO);
    }

    @Override
    public void delConfig(long id, long updateUid) {
        ConfigDO dbConfigDO = getConfig(id);
        Optional.ofNullable(dbConfigDO).map(c -> {
            c.setDeleted(true);
            c.setUpdateTime(LocalDateTime.now());
            c.setUpdateUid(updateUid);
            return c;
        }).ifPresent(this::updateConfigDO);
    }

    @Override
    public ConfigDO getConfig(long id) {
        List<ConfigDO> configList = getAllConfig();
        return configList.stream().filter(c -> c.getId() == id).findFirst().orElse(null);
    }

    @Override
    public List<ConfigDO> getAllValidConfig() {
        return getAllConfig().stream().filter(c -> !c.isDeleted()).collect(Collectors.toList());
    }

    @Override
    public List<ConfigDO> getAllConfig() {
        File[] files;
        File folder = new File(standalonePath + "/config");
        if (!folder.exists() || (files = folder.listFiles()) == null) {
            return new ArrayList<>();
        }
        return Arrays.stream(files).map(File::getAbsolutePath)
                .filter(p -> p.endsWith(".conf")).map(this::buildConfigDO)
                .filter(Objects::nonNull).sorted(Comparator.comparing(ConfigDO::getId)).collect(Collectors.toList());
    }

    private synchronized ConfigDO buildConfigDO(String path) {
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(path));
            String json = new String(bytes, StandardCharsets.UTF_8);
            return JSON.parseObject(json, ConfigDO.class);
        } catch (Exception e) {
            log.error("buildConfigDO error,path:{}", path, e);
            return null;
        }
    }

    private synchronized void updateConfigDO(ConfigDO configDO) {
        Path path = Paths.get(standalonePath + "/config/" + configDO.getId() + ".conf");
        if (Files.exists(path)) {
            try {
                Files.write(path, JSON.toJSONString(configDO).getBytes(StandardCharsets.UTF_8), StandardOpenOption.WRITE);
            } catch (IOException e) {
                log.error("updateConfigDO error configDO:{}", configDO, e);
            }
        }
    }
}
