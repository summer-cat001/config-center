package com.config.center.dao;

import com.config.center.model.ConfigDO;

import java.util.List;

/**
 * @author caojiancheng
 * @date 2024-02-08 15:09
 */
public interface ConfigDAO {
    /**
     * 新增配置
     *
     * @param configDO 配置对象
     * @return 配置id
     */
    long insertConfigDO(ConfigDO configDO);

    /**
     * 更新配置
     *
     * @param configDO 配置对象
     */
    void updateConfig(ConfigDO configDO);

    /**
     * 删除配置
     *
     * @param id        配置id
     * @param updateUid 更新用户id
     */
    void delConfig(long id, long updateUid);

    /**
     * 获取配置
     *
     * @param id 配置id
     * @return 配置对象
     */
    ConfigDO getConfig(long id);

    /**
     * 获取全部有效的配置
     *
     * @return 配置对象
     */
    List<ConfigDO> getAllValidConfig();

    /**
     * 获取全部配置
     *
     * @return 配置对象
     */
    List<ConfigDO> getAllConfig();
}
