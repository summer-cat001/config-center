package com.config.center.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author caojiancheng
 * @date 2024-02-08 18:11
 */
public class DateUtil {

    private DateUtil() {
        throw new IllegalStateException("Utility class");
    }

    private static final DateTimeFormatter FORMATTER_1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static String date2str1(LocalDateTime localDateTime) {
        return localDateTime.format(FORMATTER_1);
    }
}
