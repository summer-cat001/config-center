package com.config.center.service;

/**
 * @author caojiancheng
 * @date 2024-03-05 15:42
 */
public interface ConfigSyncService {

    /**
     * 发布事件
     *
     * @param configId 配置id
     */
    void publish(long configId);

    /**
     * 消费事件
     *
     * @param configId 配置id
     */
    void consume(long configId);
}
