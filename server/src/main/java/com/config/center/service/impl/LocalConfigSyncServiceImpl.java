package com.config.center.service.impl;

import com.config.center.service.ConfigService;
import com.config.center.service.ConfigSyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author caojiancheng
 * @date 2024-03-05 15:44
 */
@Service
public class LocalConfigSyncServiceImpl implements ConfigSyncService {

    @Autowired
    private ConfigService configService;

    @Override
    public void publish(long configId) {
        consume(configId);
    }

    @Override
    public void consume(long configId) {
        configService.onChangeConfigEvent(configId);
    }
}
