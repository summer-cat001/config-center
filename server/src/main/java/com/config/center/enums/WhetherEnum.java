package com.config.center.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @author caojiancheng
 * @date 2022-05-23 22:46
 */
@Getter
@AllArgsConstructor
public enum WhetherEnum {

    NO(0, "否"),
    YES(1, "是");

    private int value;
    private String desc;


    public static WhetherEnum getWhetherEnum(int value) {
        return Arrays.stream(WhetherEnum.values()).filter(e -> e.getValue() == value).findFirst().orElse(null);
    }

    public static WhetherEnum getWhetherEnum(String desc) {
        return Arrays.stream(WhetherEnum.values()).filter(e -> e.getDesc().equals(desc)).findFirst().orElse(null);
    }

    public static int check(boolean res) {
        return res ? WhetherEnum.YES.getValue() : WhetherEnum.NO.getValue();
    }
}
