package com.config.center.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @author caojiancheng
 * @date 2024-02-08 17:40
 */
@Getter
@AllArgsConstructor
public enum ConfigCenterModeEnum {
    STANDALONE(0, "单机模式"),
    CLUSTER(1, "集群模式");

    private int mode;
    private String message;

    public static ConfigCenterModeEnum getEnum(int mode) {
        return Arrays.stream(ConfigCenterModeEnum.values())
                .filter(e -> e.getMode() == mode).findFirst().orElse(null);
    }
}
