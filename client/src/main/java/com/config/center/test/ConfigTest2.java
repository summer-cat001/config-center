package com.config.center.test;

import com.config.center.annotation.ConfigRefresh;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author caojiancheng
 * @date 2024-03-08 16:48
 */
@Component
@ConfigRefresh
@ConfigurationProperties(prefix = "user")
public class ConfigTest2 {
    private String name;
    private int age;
    private List<String> education;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<String> getEducation() {
        return education;
    }

    public void setEducation(List<String> education) {
        this.education = education;
    }
}

