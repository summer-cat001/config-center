package com.config.center.test;

import com.config.center.annotation.ConfigRefresh;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author caojiancheng
 * @date 2024-03-08 16:48
 */
@Data
@Component
public class ConfigTest {

    @ConfigRefresh
    @Value("${user.name}")
    private String name;

}
