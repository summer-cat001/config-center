package com.config.center.annotation;

import java.lang.annotation.*;

/**
 * @author caojiancheng
 * @date 2023-11-21 17:22
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ConfigRefresh {
}
