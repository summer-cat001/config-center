package com.config.center.autoconfigure;

import com.config.center.config.ConfigRefreshAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @author caojiancheng
 * @date 2024-03-08 14:07
 */
@Import(ConfigAutoConfiguration.class)
public class ConfigAutoConfiguration implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{ConfigRefreshAnnotationBeanPostProcessor.class.getName()};
    }
}
