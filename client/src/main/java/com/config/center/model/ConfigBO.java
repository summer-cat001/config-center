package com.config.center.model;

import lombok.Data;

import java.lang.reflect.Field;
import java.util.List;

/**
 * @author caojiancheng
 * @date 2024-03-04 16:30
 */
@Data
public class ConfigBO {
    /**
     * 配置id
     */
    private long id;

    /**
     * 配置版本号
     */
    private int version;

    /**
     * 配置项列表
     */
    private List<ConfigDataBO> configDataList;
}
