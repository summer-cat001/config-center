package com.config.center.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.lang.reflect.Field;

/**
 * @author caojiancheng
 * @date 2024-03-08 16:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RefreshFieldValueBO extends RefreshFieldBO {
    /**
     * 值
     */
    private String value;

    public RefreshFieldValueBO(Object bean, Field field, String value) {
        super(bean, field);
        this.value = value;
    }
}
