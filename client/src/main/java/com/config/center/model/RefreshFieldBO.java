package com.config.center.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.reflect.Field;

/**
 * @author caojiancheng
 * @date 2024-03-04 18:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RefreshFieldBO {
    /**
     * 对象实例
     */
    private Object bean;

    /**
     * 字段
     */
    private Field field;
}
