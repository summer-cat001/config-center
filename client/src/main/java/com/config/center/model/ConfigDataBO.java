package com.config.center.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author caojiancheng
 * @date 2024-03-04 16:30
 */
@Data
public class ConfigDataBO {

    /**
     * 配置key
     */
    private String key;

    /**
     * 配置值
     */
    private String value;

    /**
     * 自动刷新的bean字段列表
     */
    List<RefreshFieldBO> refreshFieldList;

    public ConfigDataBO(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public void addRefreshField(RefreshFieldBO refreshFieldBO) {
        Optional.ofNullable(refreshFieldList).orElseGet(() -> refreshFieldList = new ArrayList<>()).add(refreshFieldBO);
    }
}
