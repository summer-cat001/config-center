package com.config.center.test;

import com.config.center.ConfigCenterClient;
import com.config.center.annotation.ConfigRefresh;
import com.config.center.model.RefreshFieldBO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author caojiancheng
 * @date 2024-03-04 14:15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ConfigApplication.class)
public class ClientTest {

    @Autowired
    private Environment environment;

    @Autowired
    private User user;

    @Autowired
    private ConfigTest configTest;

    @Autowired
    private ConfigTest2 configTest2;

    @Test
    public void configTest() throws InterruptedException {
        while (true) {
            System.out.println(configTest2.getName() + "-" + configTest2.getAge() + "-" + configTest2.getEducation());
            Thread.sleep(1000);
        }
    }


    private String userName;

    private String userAge;

    private List<Object> education;

    public ClientTest() throws NoSuchFieldException {
        ConfigCenterClient configCenterClient = ConfigCenterClient.getInstance("http://localhost:8088");
        Map<String, Object> configProperty = configCenterClient.getConfigProperty();
        this.userName = configProperty.get("user.name") + "";
        this.userAge = configProperty.get("user.age") + "";
        this.education = new ArrayList<>();
        int i = 0;
        while (configProperty.containsKey("user.education[" + i + "]")) {
            education.add(configProperty.get("user.education[" + (i++) + "]"));
        }

        configCenterClient.addRefreshField("user.name", new RefreshFieldBO(this, ClientTest.class.getDeclaredField("userName")));
        configCenterClient.startLongPolling();
    }

    public String toString() {
        return "姓名:" + userName + ",年龄:" + userAge + ",教育经历:" + education;
    }

    public static void main(String[] args) throws NoSuchFieldException, InterruptedException {
        ClientTest clientTest = new ClientTest();
        while (!Thread.interrupted()) {
            System.out.println(clientTest);
            Thread.sleep(1000);
        }
    }
}
