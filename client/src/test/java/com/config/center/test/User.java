package com.config.center.test;


/**
 * @author caojiancheng
 * @date 2024-03-08 11:13
 */
public class User {
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
